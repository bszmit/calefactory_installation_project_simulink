%*************** CONST *****************
% ogolne
xsi = 1000; % kg/m^3 - gestosc wody
cw = 4200;  % J/kg/K - cieplo wlasciwe wody

% kaloryfer
mh = 3000;  % kg     - masa zastepcza kaloryferow
ch = 2700;  % J/kg/K - zastepcze cieplo wlasciwe kaloryferow
kh = 12000; % J/K/s  - wspolczynnik przenikania ciepla kaloryfer - pokoj
Fcob_max = 40 / 3600;	% m^3/s  - maksymalny pobor wody w budynku

% budynek
mb = 20000;   % kg - masa powietrza w budynku
cb = 1000;    % J/kg/K - cieplo wlasciwe powietrza
kext = 15000; % J/K/s - wspolczynnik utraty ciepla

%**************** CONST ENDS *****************

%*************** PARAMS *****************
%To = 273 + 10;   % K - temperatura zewnetrzna
%************* PARAMS ENDS **************

%*************** INPUTS *****************
%Tzco = 30 + 273; % K - temperatura wody wplywajacej
%************* INPUTS ENDS ***************

%*************** STATE *****************
%Tr = 20 + 273;   % K - usredniona temperatura pomieszczen(w kelwinach!)
%************* STATE ENDS **************

%*************** OUTPUTS *****************
%Tpco = 25 + 273; % K - wyjscie budynku. Temperatura wody wyplywajacej z kaloryfera.
%************* OUTPUTS ENDS ***************



